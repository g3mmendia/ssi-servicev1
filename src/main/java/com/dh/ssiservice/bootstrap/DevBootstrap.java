//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.bootstrap;

import com.dh.ssiservice.model.*;
import com.dh.ssiservice.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent>
{
    private CategoryRepository categoryRepository;
    private ContractRepository contractRepository;
    private EmployeeRepository employeeRepository;
    private ItemRepository itemRepository;
    private PositionRepository positionRepository;
    private SubCategoryRepository subCategoryRepository;
    private IncidenRegistryRepository incidenRegistryRepository;
    private IncidenteRepository incidenteRepository;
    private InventoryRepository inventoryRepository;
    private OrderBuyRepository orderBuyRepository;

    public DevBootstrap(CategoryRepository categoryRepository, ContractRepository contractRepository,
                        EmployeeRepository employeeRepository, ItemRepository itemRepository,
                        PositionRepository positionRepository, SubCategoryRepository subCategoryRepository,
                        IncidenRegistryRepository incidenRegistryRepository, IncidenteRepository incidenteRepository,
                        InventoryRepository inventoryRepository, OrderBuyRepository orderBuyRepository)
    {
        this.categoryRepository = categoryRepository;
        this.contractRepository = contractRepository;
        this.employeeRepository = employeeRepository;
        this.itemRepository = itemRepository;
        this.positionRepository = positionRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.incidenRegistryRepository = incidenRegistryRepository;
        this.incidenteRepository = incidenteRepository;
        this.inventoryRepository = inventoryRepository;
        this.orderBuyRepository = orderBuyRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)
    {
        initData();
    }

    private void initData()
    {
        Category eppCategory = new Category();
        eppCategory.setCode("EPP");
        eppCategory.setName("EPP");

        categoryRepository.save(eppCategory);

//        RES category
        Category resourceCategory = new Category();
        resourceCategory.setCode("RES");
        resourceCategory.setName("RESOURCE");
        categoryRepository.save(resourceCategory);

        // safety subcategory
        SubCategory safetySubCategory = new SubCategory();
        safetySubCategory.setCategory(eppCategory);
        safetySubCategory.setCode("SAF");
        safetySubCategory.setName("SAFETY");

        subCategoryRepository.save(safetySubCategory);

        // raw material subcategory
        SubCategory rawMaterialSubCategory = new SubCategory();
        rawMaterialSubCategory.setCategory(resourceCategory);
        rawMaterialSubCategory.setCode("RM");
        rawMaterialSubCategory.setName("RAW MATERIAL");

        subCategoryRepository.save(rawMaterialSubCategory);

        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("HELMET");
        helmet.setSubCategory(safetySubCategory);

        itemRepository.save(helmet);

        // ink Item
        Item ink = new Item();
        ink.setCode("INK");
        ink.setName("INK");
        ink.setSubCategory(rawMaterialSubCategory);
        itemRepository.save(ink);

        // John Employee
        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Doe");

        // Position
        Position position = new Position();
        position.setName("OPERATIVE");
        positionRepository.save(position);

        // contract
        Contract contract = new Contract();
        contract.setEmployee(john);
        contract.setInitDate(new Date(2010, 1, 1));
        contract.setPosition(position);

        john.getContracts().add(contract);
        employeeRepository.save(john);

        contract.setInitDate(new Date(0, 1, 1));

        contractRepository.save(contract);

        //OrderBuy
        OrderBuy orderBuy = new OrderBuy();
        orderBuy.setUnidad("Electricity");
        orderBuy.setProveedor("Campero");
        orderBuy.setEstado("apro");
        orderBuy.setItem(ink);
        orderBuyRepository.save(orderBuy);

        //Inventory
        Inventory inventory = new Inventory();
        inventory.setWarehouse("Alamacen1");
        inventory.setCant(100);
        inventory.setEstado("new");
        inventory.setItem(ink);
        inventoryRepository.save(inventory);

        //Incidente
        Incidente incidente = new Incidente();
        incidente.setCode("inc001");
        incidente.setNombre("piso mojado");
        incidente.setTypeIncidente("level");
        incidenteRepository.save(incidente);

        //IncidenRegistry
        IncidenRegistry incidenRegistry = new IncidenRegistry();
        incidenRegistry.setCausa("descuido");
        incidenRegistry.setLevel("Level");
        incidenRegistry.setDateIncident(new Date(2010, 1, 1));
        incidenRegistry.setEmployee(john);
        incidenRegistry.setIncidente(incidente);
        incidenRegistryRepository.save(incidenRegistry);

    }

}
