//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.OrderBuy;

public class OrderBuyCommand
{
    private String unidad;
    private String proveedor;
    private String estado;
    private Long id;

    public OrderBuyCommand(OrderBuy orderBuy)
    {
        this.setId(orderBuy.getId());
        this.setUnidad(orderBuy.getUnidad());
        this.setProveedor(orderBuy.getProveedor());
        this.setEstado(orderBuy.getEstado());
    }

    public OrderBuyCommand()
    {

    }

    public String getUnidad()
    {
        return unidad;
    }

    public void setUnidad(String unidad)
    {
        this.unidad = unidad;
    }

    public String getProveedor()
    {
        return proveedor;
    }

    public void setProveedor(String proveedor)
    {
        this.proveedor = proveedor;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public OrderBuy toDomain()
    {
        OrderBuy orderBuy = new OrderBuy();
        orderBuy.setId(getId());
        orderBuy.setUnidad(getUnidad());
        orderBuy.setProveedor(getProveedor());
        orderBuy.setEstado(getEstado());
        return orderBuy;
    }
}