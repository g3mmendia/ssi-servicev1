//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.IncidenRegistry;

import java.util.Date;

public class IncidentRegistryCommand
{
    private Long Id;
    private Long incidenteId;
    private Date dateIncident;
    private Long employeeId;
    private String causa;
    private String level;

    public IncidentRegistryCommand(IncidenRegistry incidenRegistry)
    {
        this.setId(incidenRegistry.getId());
        this.setCausa(incidenRegistry.getCausa());
        this.setLevel(incidenRegistry.getLevel());
        this.setDateIncident(incidenRegistry.getDateIncident());
        this.setIncidenteId(incidenRegistry.getIncidente().getId());
        this.setEmployeeId(incidenRegistry.getEmployee().getId());
    }

    public IncidentRegistryCommand()
    {
    }

    public Long getIncidenteId()
    {
        return incidenteId;
    }

    public void setIncidenteId(Long incidenteId)
    {
        this.incidenteId = incidenteId;
    }

    public Date getDateIncident()
    {
        return dateIncident;
    }

    public void setDateIncident(Date dateIncident)
    {
        this.dateIncident = dateIncident;
    }

    public Long getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this.employeeId = employeeId;
    }

    public String getCausa()
    {
        return causa;
    }

    public void setCausa(String causa)
    {
        this.causa = causa;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }

    public Long getId()
    {
        return Id;
    }

    public void setId(Long id)
    {
        Id = id;
    }

    public IncidenRegistry toDomain()
    {
        IncidenRegistry incidenRegistry = new IncidenRegistry();
        incidenRegistry.setId(getId());
        incidenRegistry.setCausa(getCausa());
        incidenRegistry.setLevel(getLevel());
        incidenRegistry.setDateIncident(getDateIncident());
        return incidenRegistry;
    }
}