//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Inventory;

public class InventoryCommand
{
    private String warehouse;
    private String estado; //DNU, New, Usado, Asignado, Dispo
    private Integer cant;
    private Long id;

    public InventoryCommand(Inventory inventory)
    {
        this.setId(inventory.getId());
        this.setWarehouse(inventory.getWarehouse());
        this.setCant(inventory.getCant());
        this.setEstado(inventory.getEstado());
    }

    public InventoryCommand()
    {
    }

    public String getWarehouse()
    {
        return warehouse;
    }

    public void setWarehouse(String warehouse)
    {
        this.warehouse = warehouse;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Integer getCant()
    {
        return cant;
    }

    public void setCant(Integer cant)
    {
        this.cant = cant;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Inventory toDomain()
    {
        Inventory inventory = new Inventory();
        inventory.setId(getId());
        inventory.setWarehouse(getWarehouse());
        inventory.setCant(getCant());
        inventory.setEstado(getEstado());
        return inventory;
    }
}