//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Incidente;

public class IncidenteCommand
{
    private Long id;
    private String code;
    private String nombre;
    private String typeIncidente;

    public IncidenteCommand(Incidente incidente)
    {
        this.setId(incidente.getId());
        this.setCode(incidente.getCode());
        this.setNombre(incidente.getNombre());
        this.setTypeIncidente(incidente.getTypeIncidente());

    }

    public IncidenteCommand()
    {
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTypeIncidente()
    {
        return typeIncidente;
    }

    public void setTypeIncidente(String typeIncidente)
    {
        this.typeIncidente = typeIncidente;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Incidente toDomain()
    {
        Incidente incidente = new Incidente();
        incidente.setId(getId());
        incidente.setCode(getCode());
        incidente.setNombre(getNombre());
        incidente.setTypeIncidente(getTypeIncidente());
        return incidente;
    }
}