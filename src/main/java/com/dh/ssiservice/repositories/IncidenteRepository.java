//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Incidente;
import org.springframework.data.repository.CrudRepository;

public interface IncidenteRepository extends CrudRepository<Incidente, Long>
{
}
