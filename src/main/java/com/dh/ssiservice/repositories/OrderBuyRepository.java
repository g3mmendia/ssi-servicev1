//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.OrderBuy;
import org.springframework.data.repository.CrudRepository;

public interface OrderBuyRepository extends CrudRepository<OrderBuy, Long>
{
}
