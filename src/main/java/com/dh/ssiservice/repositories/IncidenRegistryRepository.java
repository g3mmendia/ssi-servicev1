//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.IncidenRegistry;
import org.springframework.data.repository.CrudRepository;

public interface IncidenRegistryRepository extends CrudRepository<IncidenRegistry, Long>
{
}
