package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidentRegistryCommand;
import com.dh.ssiservice.model.IncidenRegistry;
import com.dh.ssiservice.services.EmployeeService;
import com.dh.ssiservice.services.IncidenRegistryService;
import com.dh.ssiservice.services.IncidenteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incidenregistries")
@Produces("application/json")
@CrossOrigin
public class IncidenRegistryController
{
    private IncidenRegistryService service;
    private IncidenteService incidenteService;
    private EmployeeService employeeService;

    public IncidenRegistryController(IncidenRegistryService service, IncidenteService incidenteService, EmployeeService employeeService)
    {
        this.service = service;
        this.incidenteService = incidenteService;
        this.employeeService = employeeService;
    }

    @GET
    public Response getIncidenRegistries()
    {
        List<IncidentRegistryCommand> incidenRegistries = new ArrayList<>();
        service.findAll().forEach(incidenRegistry -> {
            IncidentRegistryCommand incidenRegistryCommand = new IncidentRegistryCommand(incidenRegistry);
            incidenRegistries.add(incidenRegistryCommand);
        });
        return Response.ok(incidenRegistries).build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidenRegistriesById(@PathParam("id") @NotNull Long id)
    {
        IncidenRegistry incidenRegistry = service.findById(id);
        return Response.ok(new IncidentRegistryCommand(incidenRegistry)).build();
    }

    @POST
    public Response saveIncidenRegistry(IncidentRegistryCommand incidenRegistry)
    {
        IncidenRegistry model = incidenRegistry.toDomain();
        model.setEmployee(employeeService.findById(incidenRegistry.getEmployeeId()));
        IncidenRegistry incidenRegistryPersisted = service.save(model);
        return Response.ok(new IncidentRegistryCommand(incidenRegistryPersisted)).build();
    }

    @PUT
    public Response updateIncidenRegistry(IncidenRegistry incidenRegistry)
    {
        IncidenRegistry incidenRegistryPersisted = service.save(incidenRegistry);
        return Response.ok(new IncidentRegistryCommand(incidenRegistryPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteIncidenRegistry(@PathParam("id") String id)
    {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth()
    {
        return Response.ok().build();
    }

}  
