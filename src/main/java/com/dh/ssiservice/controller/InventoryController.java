package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.InventoryCommand;
import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.services.InventoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/inventories")
@Produces("application/json")
@CrossOrigin
public class InventoryController
{
    private InventoryService service;

    public InventoryController(InventoryService service)
    {
        this.service = service;
    }

    @GET
    public Response getInventories()
    {
        List<InventoryCommand> inventories = new ArrayList<>();
        service.findAll().forEach(inventory -> {
            InventoryCommand inventoryCommand = new InventoryCommand(inventory);
            inventories.add(inventoryCommand);
        });
        return Response.ok(inventories).build();
    }

    @GET
    @Path("/{id}")
    public Response getInventoriesById(@PathParam("id") @NotNull Long id)
    {
        Inventory inventory = service.findById(id);
        return Response.ok(new InventoryCommand(inventory)).build();
    }

    @POST
    public Response saveInventory(InventoryCommand inventory)
    {
        Inventory model = inventory.toDomain();
        Inventory inventoryPersisted = service.save(model);
        return Response.ok(new InventoryCommand(inventoryPersisted)).build();
    }

    @PUT
    public Response updateInventory(Inventory inventory)
    {
        Inventory inventoryPersisted = service.save(inventory);
        return Response.ok(new InventoryCommand(inventoryPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteInventory(@PathParam("id") String id)
    {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth()
    {
        return Response.ok().build();
    }
}  
