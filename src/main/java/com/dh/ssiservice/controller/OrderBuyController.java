package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.OrderBuyCommand;
import com.dh.ssiservice.model.OrderBuy;
import com.dh.ssiservice.services.OrderBuyService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Controller
@Path("/orderbuys")
@Produces("application/json")
@CrossOrigin
public class OrderBuyController
{
    private OrderBuyService service;

    public OrderBuyController(OrderBuyService service)
    {
        this.service = service;
    }

    @GET
    public Response getOrderBuys()
    {
        List<OrderBuyCommand> orderBuys = new ArrayList<>();
        service.findAll().forEach(orderBuy -> {
            OrderBuyCommand orderBuyCommand = new OrderBuyCommand(orderBuy);
            orderBuys.add(orderBuyCommand);
        });
        return Response.ok(orderBuys).build();
    }

    @GET
    @Path("/{id}")
    public Response getOrderBuysById(@PathParam("id") @NotNull Long id)
    {
        OrderBuy orderBuy = service.findById(id);
        return Response.ok(new OrderBuyCommand(orderBuy)).build();
    }

    @POST
    public Response saveOrderBuy(OrderBuyCommand orderBuy)
    {
        OrderBuy model = orderBuy.toDomain();
        OrderBuy orderBuyPersisted = service.save(model);
        return Response.ok(new OrderBuyCommand(orderBuyPersisted)).build();
    }

    @PUT
    public Response updateOrderBuy(OrderBuy orderBuy)
    {
        OrderBuy orderBuyPersisted = service.save(orderBuy);
        return Response.ok(new OrderBuyCommand(orderBuyPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteOrderBuy(@PathParam("id") String id)
    {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth()
    {
        return Response.ok().build();
    }
}  
