package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidenteCommand;
import com.dh.ssiservice.model.Incidente;
import com.dh.ssiservice.services.IncidenteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incidentes")
@Produces("application/json")
@CrossOrigin
public class IncidenteController
{
    private IncidenteService service;

    public IncidenteController(IncidenteService service)
    {
        this.service = service;
    }

    @GET
    public Response getIncidentes()
    {
        List<IncidenteCommand> incidentes = new ArrayList<>();
        service.findAll().forEach(incidente -> {
            IncidenteCommand incidenteCommand = new IncidenteCommand(incidente);
            incidentes.add(incidenteCommand);
        });
        return Response.ok(incidentes).build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentesById(@PathParam("id") @NotNull Long id)
    {
        Incidente incidente = service.findById(id);
        return Response.ok(new IncidenteCommand(incidente)).build();
    }

    @POST
    public Response saveIncidente(IncidenteCommand incidente)
    {
        Incidente model = incidente.toDomain();
        Incidente incidentePersisted = service.save(model);
        return Response.ok(new IncidenteCommand(incidentePersisted)).build();
    }

    @PUT
    public Response updateIncidente(Incidente incidente)
    {
        Incidente incidentePersisted = service.save(incidente);
        return Response.ok(new IncidenteCommand(incidentePersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteIncidente(@PathParam("id") String id)
    {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth()
    {
        return Response.ok().build();
    }

}  
