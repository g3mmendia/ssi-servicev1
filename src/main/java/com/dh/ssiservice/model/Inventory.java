//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Inventory extends ModelBase
{
    private String warehouse;
    private String estado; //DNU, New, Usado, Asignado, Dispo
    private Integer cant;

    @OneToOne(optional = false)
    private Item item;

    public String getWarehouse()
    {
        return warehouse;
    }

    public void setWarehouse(String warehouse)
    {
        this.warehouse = warehouse;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Integer getCant()
    {
        return cant;
    }

    public void setCant(Integer cant)
    {
        this.cant = cant;
    }

    public Item getItem()
    {
        return item;
    }

    public void setItem(Item item)
    {
        this.item = item;
    }
}