//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.model;

import javax.persistence.Entity;

@Entity
public class Incidente extends ModelBase
{
    private String code;
    private String nombre;
    private String typeIncidente;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTypeIncidente()
    {
        return typeIncidente;
    }

    public void setTypeIncidente(String typeIncidente)
    {
        this.typeIncidente = typeIncidente;
    }
}