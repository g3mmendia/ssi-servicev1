//--------Marvin Dickson Mendia Calizaya-------
package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class IncidenRegistry extends ModelBase
{
    @OneToOne(optional = false)
    private Incidente incidente;
    private Date dateIncident;
    @OneToOne(optional = false)
    private Employee employee;
    private String causa;
    private String level; //low,medium,high

    public Incidente getIncidente()
    {
        return incidente;
    }

    public void setIncidente(Incidente incidente)
    {
        this.incidente = incidente;
    }

    public Date getDateIncident()
    {
        return dateIncident;
    }

    public void setDateIncident(Date dateIncident)
    {
        this.dateIncident = dateIncident;
    }

    public Employee getEmployee()
    {
        return employee;
    }

    public void setEmployee(Employee employee)
    {
        this.employee = employee;
    }

    public String getCausa()
    {
        return causa;
    }

    public void setCausa(String causa)
    {
        this.causa = causa;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }
}