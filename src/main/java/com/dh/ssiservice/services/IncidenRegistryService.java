package com.dh.ssiservice.services;

import com.dh.ssiservice.model.IncidenRegistry;

public interface IncidenRegistryService extends GenericService<IncidenRegistry>
{
}
