package com.dh.ssiservice.services;

import com.dh.ssiservice.model.OrderBuy;
import com.dh.ssiservice.repositories.OrderBuyRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderBuyServiceImpl extends GenericServiceImpl<OrderBuy> implements OrderBuyService
{
    private OrderBuyRepository repository;

    public OrderBuyServiceImpl(OrderBuyRepository repository)
    {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<OrderBuy, Long> getRepository()
    {
        return repository;
    }
}
