package com.dh.ssiservice.services;

import com.dh.ssiservice.model.OrderBuy;

public interface OrderBuyService extends GenericService<OrderBuy>
{
}
