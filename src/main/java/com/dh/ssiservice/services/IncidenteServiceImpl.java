package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Incidente;
import com.dh.ssiservice.repositories.IncidenteRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidenteServiceImpl extends GenericServiceImpl<Incidente> implements IncidenteService
{
    private IncidenteRepository repository;

    public IncidenteServiceImpl(IncidenteRepository repository)
    {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Incidente, Long> getRepository()
    {
        return repository;
    }
}
