package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Incidente;

public interface IncidenteService extends GenericService<Incidente>
{
}
