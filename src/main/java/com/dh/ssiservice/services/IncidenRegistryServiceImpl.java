package com.dh.ssiservice.services;

import com.dh.ssiservice.model.IncidenRegistry;
import com.dh.ssiservice.repositories.IncidenRegistryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidenRegistryServiceImpl extends GenericServiceImpl<IncidenRegistry> implements IncidenRegistryService
{
    private IncidenRegistryRepository repository;

    public IncidenRegistryServiceImpl(IncidenRegistryRepository repository)
    {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<IncidenRegistry, Long> getRepository()
    {
        return repository;
    }
}
